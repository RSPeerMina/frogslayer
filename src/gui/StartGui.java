package gui;

import org.rspeer.ui.Log;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class StartGui extends Gui {
    private boolean started = false;
    public static int atkGoal, strGoal, defGoal;
    private JTextField atkField, strField, defField;
    private final JCheckBox buryBones = new JCheckBox("Bury bones?");

    private void buryBonesActionPerformed(ActionEvent e) { buryBones.isSelected(); }

    public JCheckBox getBuryBones() { return buryBones; }

    @SuppressWarnings("unchecked")
    public StartGui(){
        super();
        mainPanel.setBackground(Color.BLACK);
        mainPanel.setForeground(Color.WHITE);
        mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(mainPanel);
        mainPanel.setLayout(null);

        JLabel lblAtk = new JLabel("Attack");
        lblAtk.setBounds(10, 11, 84, 14);
        atkField = new JTextField();
        atkField.setBounds(93, 8, 124, 20);
        mainPanel.add(lblAtk);
        mainPanel.add(atkField);

        JLabel lblStr = new JLabel("Strength");
        lblStr.setBounds(10, 36, 73, 14);
        strField = new JTextField();
        strField.setBounds(93, 33, 124, 20);
        mainPanel.add(lblStr);
        mainPanel.add(strField);

        JLabel lblDef = new JLabel("Defense");
        lblDef.setBounds(10, 61, 84, 14);
        defField = new JTextField();
        defField.setBounds(93, 58, 124, 20);
        mainPanel.add(lblDef);
        mainPanel.add(defField);

        buryBones.setBounds(93, 83, 124, 20);
        mainPanel.add(buryBones);
        buryBones.addActionListener(this::buryBonesActionPerformed);


        JButton btnStartBot = new JButton("Start");
        btnStartBot.setForeground(Color.CYAN);
        btnStartBot.setBounds(93, 108, 89, 23);
        btnStartBot.addActionListener(e -> {
            atkGoal = Integer.parseInt(atkField.getText());
            strGoal = Integer.parseInt(strField.getText());
            defGoal = Integer.parseInt(defField.getText());
            started = true;
            setVisible(false);
            Log.fine("Attack goal: " + atkGoal());
            Log.fine("Strength goal: " + strGoal());
            Log.fine("Defense goal: " + defGoal());

        });
        mainPanel.add(btnStartBot);
        setVisible(true);
    }

    public boolean isStarted(){
        return started;
    }

    public int atkGoal(){
        return atkGoal;
    }

    public int strGoal(){
        return strGoal;
    }

    public int defGoal(){
        return defGoal;
    }
}
