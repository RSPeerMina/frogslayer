package gui;

import org.rspeer.runetek.api.Game;

import javax.swing.*;
import java.awt.*;

public class Gui extends JFrame {

    protected final JPanel mainPanel = new JPanel(new GridBagLayout());

    public Gui(){
        setTitle("FrogSlayer");
        setSize(241, 175);
        setLocationRelativeTo(Game.getCanvas());
        setBackground(Color.CYAN);
        setResizable(false);

        add(mainPanel);
    }
}
