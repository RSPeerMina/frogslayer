import gui.StartGui;
import org.rspeer.runetek.adapter.Interactable;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.tab.*;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.event.listeners.DeathListener;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.DeathEvent;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.ui.Log;

import java.awt.*;

import static gui.StartGui.*;

@ScriptMeta(
        name = "FrogSlayer ",
        desc = "Kills Frogs in lumbridge swamp, death walks.",
        developer = "Mina",
        category = ScriptCategory.COMBAT
)

public class FrogSlayer extends Script implements RenderListener, DeathListener {

    StopWatch timer;
    private static Position sw = new Position(3200, 3175);
    private static StartGui startGui;
    private int head, ammy, top, leg, boot, wep, glove, ring, shield, cape, startAtk, startStr, startDef;

    public boolean buryBones() {
        return startGui.getBuryBones().isSelected();
    }

    public void onStart() {
        timer = StopWatch.start();

        head = EquipmentSlot.HEAD.getItemId();
        ammy = EquipmentSlot.NECK.getItemId();
        top = EquipmentSlot.CHEST.getItemId();
        leg = EquipmentSlot.LEGS.getItemId();
        boot = EquipmentSlot.FEET.getItemId();
        wep = EquipmentSlot.MAINHAND.getItemId();
        glove = EquipmentSlot.HANDS.getItemId();
        ring = EquipmentSlot.RING.getItemId();
        shield = EquipmentSlot.OFFHAND.getItemId();
        cape = EquipmentSlot.CAPE.getItemId();
        startAtk = Skills.getExperience(Skill.ATTACK);
        startStr = Skills.getExperience(Skill.STRENGTH);
        startDef = Skills.getExperience(Skill.DEFENCE);

        startGui = new StartGui();
    }

    private boolean isAnimating() {
        return Players.getLocal().isAnimating();
    }

    private boolean isMoving() {
        return Players.getLocal().isMoving();
    }

    private void kill() {
        Npc f1 = Npcs.getNearest(x -> x.getName().equals("Frog")
                && (x.getTargetIndex() == -1 || x.getTarget().equals(Players.getLocal()))
                && x.getHealthPercent() > 0);
        Npc f2 = Npcs.getNearest(x -> x.getName().equals("Big frog")
                && (x.getTargetIndex() == -1 || x.getTarget().equals(Players.getLocal()))
                && x.getHealthPercent() > 0);
        Npc f3 = Npcs.getNearest(x -> x.getName().contains("rog")
                && (x.getTargetIndex() == -1 || x.getTarget().equals(Players.getLocal()))
                && x.getHealthPercent() > 0);

        if (Players.getLocal().getTargetIndex() < 0) {
            if (Players.getLocal().getCombatLevel() <= 10) {
                if (f1 != null) {
                    if (!isMoving() && !isAnimating()) {
                        f1.interact("Attack");
                        Time.sleepUntil(() -> isAnimating(), Random.low(750, 1500), Random.low(2000, 2500));
                    }
                }
            } else {
                if (Players.getLocal().getCombatLevel() > 10 && Players.getLocal().getCombatLevel() <= 20) {
                    if (f2 != null) {
                        if (!isMoving() && !isAnimating()) {
                            f2.interact("Attack");
                            Time.sleepUntil(() -> isAnimating(), Random.low(750, 1500), Random.low(2000, 2500));
                        }
                    }
                } else {
                    if (f3 != null) {
                        if (!isMoving() && !isAnimating()) {
                            f3.interact("Attack");
                            Time.sleepUntil(() -> isAnimating(), Random.low(750, 1500), Random.low(2000, 2500));
                        }
                    }
                }
            }
        }
    }

    private String formatTime(final long ms) {
        long s = ms / 1000, m = s / 60, h = m / 60;
        s %= 60;
        m %= 60;
        h %= 24;
        return String.format("%02d:%02d:%02d", h, m, s);
    }

    public void notify(RenderEvent m) {

        int atkGained = Skills.getExperience(Skill.ATTACK) - startAtk;
        int strGained = Skills.getExperience(Skill.STRENGTH) - startStr;
        int defGained = Skills.getExperience(Skill.DEFENCE) - startDef;

        Graphics g = m.getSource();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        int y = 35;
        int x = 10;

        g2.setColor(new Color(0.0f, 0.0f, 0.0f, 0.5f));
        g2.setColor(Color.MAGENTA);
        switch (Combat.getAttackStyle()) {
            case ACCURATE:
                g2.drawString("Currently training: Attack (" + (int) timer.getHourlyRate(atkGained) + (" XP/h)"), x, y);
                break;
            case AGGRESSIVE:
                g2.drawString("Currently training: Strength (" + (int) timer.getHourlyRate(strGained) + (" XP/h)"), x, y);
                break;
            case DEFENSIVE:
                g2.drawString("Currently training: Defence (" + (int) timer.getHourlyRate(defGained) + (" XP/h)"), x, y);
                break;
        }

        switch (Combat.getAttackStyle()) {
            case ACCURATE:
                int xpGained = atkGained;
                int nextLvlXp = Skills.getExperienceToNextLevel(Skill.ATTACK);
                double ttl = (nextLvlXp / (timer.getHourlyRate(xpGained) / 60.0 / 60.0 / 1000.0));
                if (xpGained == 0) {
                    ttl = 0;
                }
                g2.drawString("Next level in: " + formatTime(Double.valueOf(ttl).longValue()), x, y += 15);
                break;
            case AGGRESSIVE:
                xpGained = strGained;
                nextLvlXp = Skills.getExperienceToNextLevel(Skill.STRENGTH);
                ttl = (nextLvlXp / (timer.getHourlyRate(xpGained) / 60.0 / 60.0 / 1000.0));
                if (xpGained == 0) {
                    ttl = 0;
                }
                g2.drawString("Next level in: " + formatTime(Double.valueOf(ttl).longValue()), x, y += 15);
                break;
            case DEFENSIVE:
                xpGained = defGained;
                nextLvlXp = Skills.getExperienceToNextLevel(Skill.DEFENCE);
                ttl = (nextLvlXp / (timer.getHourlyRate(xpGained) / 60.0 / 60.0 / 1000.0));
                if (xpGained == 0) {
                    ttl = 0;
                }
                g2.drawString("Next level in: " + formatTime(Double.valueOf(ttl).longValue()), x, y += 15);
                break;
        }

        g2.drawString("Attack: " + Skills.getLevel(Skill.ATTACK), x, y += 15);
        g2.drawString("Strength: " + Skills.getLevel(Skill.STRENGTH), x, y += 15);
        g2.drawString("Defence: " + Skills.getLevel(Skill.DEFENCE), x, y += 15);
        if (buryBones()) {
            g2.drawString("Prayer: " + Skills.getLevel(Skill.PRAYER), x, y += 15);
        }
    }

    public void notify(DeathEvent e) {
        if (e.getSource().equals(Players.getLocal())) {
            sw = Players.getLocal().getPosition();
        }
    }

    public int loop() {
        this.setTitlePaneMessage("Runtime: " + timer.toElapsedString());
        Interactable i = Inventory.getFirst(head, ammy, top, leg, boot, wep, glove, ring, shield, cape);
        Pickable g = Pickables.getNearest(head, ammy, top, leg, boot, wep, glove, ring, shield, cape);

        if (Movement.getRunEnergy() >= Random.low(15, 30) && !Movement.isRunEnabled()) {
            Movement.toggleRun(true);
            return 1000;
        }

        if (Dialog.canContinue()) {
            Dialog.processContinue();
        }

        if (startGui.isStarted()) {
            if (!Combat.isAutoRetaliateOn()) {
                Combat.toggleAutoRetaliate(true);
                Log.info("Auto retaliate is now on");
            }

            if (((Skills.getLevel(Skill.STRENGTH) >= Skills.getLevel(Skill.DEFENCE)) || Skills.getLevel(Skill.STRENGTH) >= strGoal)
                    && Skills.getLevel(Skill.DEFENCE) <= Skills.getLevel(Skill.ATTACK)
                    && Skills.getLevel(Skill.DEFENCE) < defGoal) {
                if (Combat.getAttackStyle() != Combat.AttackStyle.DEFENSIVE) {
                    Combat.select(3);
                    Log.info("Now training Defence");
                    Time.sleepUntil(() -> Combat.getAttackStyle() == Combat.AttackStyle.DEFENSIVE, 1000, 2000);
                }
            }

            if ((Skills.getLevel(Skill.DEFENCE) > Skills.getLevel(Skill.ATTACK) || Skills.getLevel(Skill.DEFENCE) == defGoal)
                    && Skills.getLevel(Skill.ATTACK) < atkGoal) {
                if (Combat.getAttackStyle() != Combat.AttackStyle.ACCURATE) {
                    Combat.select(0);
                    Log.info("Now training Attack");
                    Time.sleepUntil(() -> Combat.getAttackStyle() == Combat.AttackStyle.ACCURATE, 1000, 2000);
                }
            }

            if (((Skills.getLevel(Skill.ATTACK) > Skills.getLevel(Skill.STRENGTH)) || Skills.getLevel(Skill.ATTACK) >= atkGoal)
                    && Skills.getLevel(Skill.STRENGTH) < strGoal) {
                if (Combat.getAttackStyle() != Combat.AttackStyle.AGGRESSIVE) {
                    Combat.select(1);
                    Log.info("Now training Strength");
                    Time.sleepUntil(() -> Combat.getAttackStyle() == Combat.AttackStyle.AGGRESSIVE, 1000, 2000);
                }
            }


            if (Skills.getLevel(Skill.ATTACK) >= atkGoal && Skills.getLevel(Skill.STRENGTH) >= strGoal && Skills.getLevel(Skill.DEFENCE) >= defGoal) {
                Log.fine("We've achieved our goal! Stopping");
                setStopping(true);
            }

            if (i != null && g == null) {
                if (i.containsAction("Wear")) {
                    i.interact("Wear");
                    Time.sleepUntil(() -> i == null, Random.low(150, 200), Random.low(300, 400));
                } else {
                    if (i.containsAction("Wield")) {
                        i.interact("Wield");
                        Time.sleepUntil(() -> i == null, Random.low(150, 200), Random.low(300, 400));
                    }
                }
            } else {
                if (sw.distance() < 30) {
                    if (g != null && !isMoving()) {
                        g.interact("Take");
                        Time.sleepUntil(() -> g == null, Random.low(250, 300), Random.low(500, 600));
                    } else {
                        if (buryBones()) {
                            Pickable b = Pickables.getNearest(x -> x.getName().contains("ones"));

                            if (b != null && b.isPositionInteractable() && b.distance() <= 5 && Players.getLocal().getTargetIndex() < 0 && !isMoving()) {
                                b.interact("Take");
                                Time.sleepUntil(() -> b == null, Random.low(200, 300), Random.low(500, 600));
                            } else {
                                if (Inventory.contains(x -> x.getName().contains("ones") && x.containsAction("Bury"))) {
                                    Inventory.getFirst(x -> x.getName().contains("ones")).interact("Bury");
                                    Time.sleepUntil(() -> !Inventory.contains(x -> x.getName().contains("ones")), Random.low(250, 350), Random.low(500, 600));
                                } else {
                                    kill();
                                }
                            }
                        } else {
                            kill();
                        }
                    }

                } else {
                    Movement.walkToRandomized(sw);
                    Time.sleepUntil(() -> sw.distance() <= 30, Random.low(1000, 1500), Random.low(2000, 3000));
                }
            }
        }

        return Random.low(500, 750);
    }

    public void onStop() {
        Log.fine("Thank you for using Frog Slayer by Mina!");
        if (Skills.getLevel(Skill.ATTACK) >= atkGoal && Skills.getLevel(Skill.STRENGTH) >= strGoal && Skills.getLevel(Skill.DEFENCE) >= defGoal) {
            Game.logout();
        }
        super.onStop();
    }

}
